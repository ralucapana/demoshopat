package com.ralucapana;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class Header {

    private final SelenideElement shoppingCartIcon = $(".fa-shopping-cart");
    private final SelenideElement favoritesIcon = $(".fa-heart");
    private final SelenideElement greetingsMessage = $(".navbar-text span span");
    private final SelenideElement shoppingBagIcon = $(".fa-shopping-bag");
    private final SelenideElement loginIcon = $(".fa-sign-in-alt");
    private final SelenideElement cartCounter = $(".shopping_cart_badge");
    private final SelenideElement favoritesCounter = $(".shopping_cart_badge");
    private final SelenideElement logoutButton = $(".fa-sign-out-alt");
    private final SelenideElement favoritesIconBadge = $(".shopping_cart_badge");
    private final SelenideElement shoppingCartIconBadge = $(".shopping_cart_badge");

    @Step("Display greetings message")
    public String getGreetingsMessage() {
        return greetingsMessage.getText();
    }

    @Step("Shopping bag icon is displayed")
    public boolean isShoppingBagIconDisplayed() {
        return shoppingBagIcon.isDisplayed();
    }

    @Step("Shopping cart icon is displayed")
    public boolean isShoppingCartIconDisplayed() {
        return shoppingCartIcon.isDisplayed();
    }

    @Step("Favorites icon is displayed")
    public boolean isFavoritesIconDisplayed() {
        return favoritesIcon.isDisplayed();
    }

    @Step("Click on the Login icon")
    public void clickOnTheLoginIcon() {
        loginIcon.click();
    }

    @Step("Display quantity on cart badge")
    public String getCartCounter() {
        return cartCounter.getText();
    }

    @Step("Display quantity on favorites badge")
    public String getFavoritesCounter() {
        return favoritesCounter.getText();
    }

    @Step("Click on the Logout button")
    public void clickOnTheLogoutButton() {
        logoutButton.click();
    }

    @Step("Click on the Shopping cart button")
    public void clickOnTheShoppingCartButton() {
        shoppingCartIcon.click();
    }

    @Step("Click on the Favorites button")
    public void clickOnTheFavoritesButton() {
        favoritesIcon.click();
    }

    @Step("Click on the Shopping bag button")
    public void clickOnTheShoppingBagIcon() {
        shoppingBagIcon.click();
    }

    @Step("Check if Favorites icon badge is displayed")
    public boolean isFavoritesIconBadgeDisplayed() {
        return favoritesIconBadge.isDisplayed();
    }

    @Step("Check if Shopping cart icon badge is displayed")
    public boolean isShoppingCartIconBadgeDisplayed() {
        return shoppingCartIconBadge.isDisplayed();
    }
}
