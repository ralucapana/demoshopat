package com.ralucapana;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class LoginModal {
    private final SelenideElement loginModalTitle = $(".modal-title small.text-muted");
    private final SelenideElement usernameField = $("#user-name");
    private final SelenideElement passwordField = $("#password");
    private final SelenideElement loginButton = $("button.btn.btn-primary");
    private final SelenideElement warningMessage = $(".error");
    private final SelenideElement instructionMessage = $(".error");
    private final SelenideElement attentionMessage = $(".error");

    @Step("Identify Login modal")
    public String getLoginModalTitle() {
        return loginModalTitle.getOwnText();
    }

    @Step("Type in {username}")
    public void typeInUsername(String username) {
        usernameField.setValue(username);
    }

    @Step("Type in {password}")
    public void typeInPassword(String password) {
        passwordField.setValue(password);
    }

    @Step("Click on the login button")
    public void clickOnTheLoginButton() {
        loginButton.click();
    }

    @Step("Warning message is generated")
    public String getWarningMessage() {
        return warningMessage.getText();
    }

    @Step("Instruction message is generated")
    public String getInstructionMessage() {
        return instructionMessage.getText();
    }

    @Step("Attention message is generated")
    public String getAttentionMessage() {
        return attentionMessage.getText();
    }


}
