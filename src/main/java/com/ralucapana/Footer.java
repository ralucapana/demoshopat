package com.ralucapana;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class Footer {
    private final SelenideElement reset = $(".fa-undo");
    private final SelenideElement aboutVersion = $(".nav-link");
    private final SelenideElement questionMark = $(".fa-question");

    @Step("Reset button is displayed")
    public boolean isResetPageDisplayed() {
        return reset.isDisplayed();
    }

    @Step("Click on Reset button")
    public void resetPage() {
        reset.click();
    }

    @Step("About version link is displayed")
    public boolean isAboutVersionDisplayed() {
        return aboutVersion.isDisplayed();
    }

    @Step("User modal button is displayed")
    public boolean isUserModalButtonDisplayed() {
        return questionMark.isDisplayed();
    }

    @Step("Click on About version")
    public void pressAboutVersionLink() {
        aboutVersion.click();
    }

    @Step("Click on Cancel button")
    public void pressCancelButton() {
        reset.click();
    }
}
