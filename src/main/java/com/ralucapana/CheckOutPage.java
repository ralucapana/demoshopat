package com.ralucapana;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class CheckOutPage {

    private final SelenideElement firstNameField = $("#first-name");

    private final SelenideElement lastNameField = $("#last-name");

    private final SelenideElement addressField = $("#address");

    private final SelenideElement continueCheckOutButton = $(".btn-success");

    private final SelenideElement cancelButton = $(".btn-danger");

    private final SelenideElement instructionMessage = $(".error");

    @Step("First name field is displayed")

    public boolean isFirstNameFieldDisplayed() {
        return firstNameField.isDisplayed();
    }

    @Step("Last name field is displayed")
    public boolean isLastNameFieldDisplayed() {
        return lastNameField.isDisplayed();
    }

    @Step("Address field is displayed")
    public boolean isAddressFieldDisplayed() {
        return addressField.isDisplayed();
    }

    @Step("Fill in {firstName}")
    public void fillInFirstName(String firstName) {
        firstNameField.setValue(firstName);
    }

    @Step("Fill in {lastName}")
    public void fillInLastName(String lastName) {
        lastNameField.setValue(lastName);
    }

    @Step("Fill in {address}")
    public void fillInAddress(String address) {
        addressField.setValue(address);
    }

    @Step("Click on continue check out button")
    public void clickOnContinueCheckOutButton() {
        continueCheckOutButton.click();
    }

    @Step("Message is generated")
    public String getInstructionMessage() {
        return instructionMessage.getText();
    }

    @Step("Click on cancel button")
    public void clickOnCancelButton() {
        cancelButton.click();
    }
}
