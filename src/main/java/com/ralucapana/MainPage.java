package com.ralucapana;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class MainPage {
    public static final String HOMEPAGE_URL = "https://fasttrackit-test.netlify.app/#/";

    private final SelenideElement searchField = $("#input-search");

    private final SelenideElement searchButton = $(".btn-light");

    private final SelenideElement sortDropdown = $(".sort-products-select");

    private final SelenideElement listContainer = $(".row.row-cols-xl-4");

    private final SelenideElement subHeader = $(".subheader-container");

    public void openPage() {
        open(HOMEPAGE_URL);
    }


    @Step("Identify Main Page")
    public String getPageTitle() {
        return Selenide.title();
    }

    @Step("Search field is displayed")
    public boolean isSearchFieldDisplayed() {
        return searchField.isDisplayed();
    }

    @Step("Search button is displayed")
    public boolean isSearchButtonDisplayed() {
        return searchButton.isDisplayed();
    }

    @Step("Type in {searchField}")
    public void typeInSearchField(String text) {
        searchField.setValue(text);
    }

    @Step("Click on the Search button")
    public void clickOnTheSearchButton() {
        searchButton.click();
    }

    @Step("Product list page is empty")
    public boolean isProductListPageEmpty() {
        return !listContainer.lastChild().exists();
    }

    @Step("Sort products")
    public void sortProducts(String value) {
        sortDropdown.selectOptionByValue(value);
    }

    @Step("Identify product list page")
    public String getSubHeaderText() {
        return subHeader.getText();
    }
}
