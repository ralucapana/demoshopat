package com.ralucapana;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class OrderSummaryPage {
    public static final String ORDER_INFO = "Payment Information:\n" +
            "Cash on delivery\n" +
            "Shipping Information:\n" +
            "CHOO CHOO DELIVERY!";
    private final SelenideElement itemsTotal;
    private final SelenideElement tax;
    private final SelenideElement totalAmount;
    private final SelenideElement paymentAndDeliveryInfo;
    private final SelenideElement cancelButton = $(".btn-danger");
    private final SelenideElement completeYourOrderButton = $(".btn-success");

    public OrderSummaryPage() {
        ElementsCollection priceDataRows = $$("table.text-right tr");
        this.itemsTotal = priceDataRows.get(0);
        this.tax = priceDataRows.get(1);
        this.totalAmount = priceDataRows.get(2);
        ElementsCollection pageColumns = $$("div .row .col");
        this.paymentAndDeliveryInfo = pageColumns.get(1);
    }

    @Step("Cancel button is displayed")
    public boolean isCancelButtonDisplayed() {
        return cancelButton.isDisplayed();
    }

    @Step("Complete your order button is displayed")
    public boolean isCompleteYourOrderButtonDisplayed() {
        return completeYourOrderButton.isDisplayed();
    }

    @Step("Items total is displayed")
    public boolean isItemsTotalDisplayed() {
        return itemsTotal.isDisplayed();
    }

    @Step("Tax is displayed")
    public boolean isTaxDisplayed() {
        return tax.isDisplayed();
    }

    @Step("Total amount is displayed")
    public boolean isTotalAmountDisplayed() {
        return totalAmount.isDisplayed();
    }

    @Step("Payment and delivery info is displayed")
    public String getPaymentAndDeliveryInformation() {
        return paymentAndDeliveryInfo.getText();
    }

    @Step("Click on Complete your order button")
    public void completeYourOrder() {
        completeYourOrderButton.click();
    }

    @Step("Click on Cancel button")
    public void cancel() {
        cancelButton.click();
    }

}
