package com.ralucapana;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class CartPage {

    private final SelenideElement welcomingMessage = $(".text-center");
    private final SelenideElement continueShoppingButton = $(".fa-angle-left");
    private final SelenideElement checkOutButton = $(".fa-angle-right");
    private final ElementsCollection priceDataRows;
    private final SelenideElement itemsTotal;
    private final SelenideElement tax;
    private final SelenideElement totalAmount;

    public CartPage() {
        SelenideElement parentColumn = $(".text-right");
        this.priceDataRows = parentColumn.$$(".amount");
        this.itemsTotal = priceDataRows.get(0);
        this.tax = priceDataRows.get(1);
        this.totalAmount = priceDataRows.get(2);
    }


    @Step("Message about adding products to cart is generated")
    public String getWelcomingMessage() {
        return welcomingMessage.getText();
    }

    @Step("Click on continue shopping button")
    public void continueShopping() {
        continueShoppingButton.click();
    }

    @Step("Click on check out button")
    public void checkOut() {
        checkOutButton.click();
    }

    @Step("Items total is displayed")
    public boolean isItemsTotalDisplayed() {
        return itemsTotal.isDisplayed();
    }

    @Step("Tax is displayed")
    public boolean isTaxDisplayed() {
        return tax.isDisplayed();
    }

    @Step("Total amount is displayed")
    public boolean isTotalAmountDisplayed() {
        return totalAmount.isDisplayed();
    }

    @Step("Items total is returned")
    public String getItemsTotal() {
        return priceDataRows.get(0).getText();
    }

    @Step("Tax is returned")
    public String getTax() {
        return priceDataRows.get(1).getText();
    }

    @Step("Total amount is returned")
    public String getTotalAmount() {
        return priceDataRows.get(2).getText();
    }

}

