package com.ralucapana;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class Product {
    private final SelenideElement description;
    private final SelenideElement link;
    private final SelenideElement addToCartButton;
    private final SelenideElement addToFavoritesButton;
    private final SelenideElement removeFromFavoritesButton;
    private final SelenideElement price;
    private final SelenideElement image;
    private final SelenideElement fullHeart;
    private final SelenideElement brokenHeart;
    private final SelenideElement inStockText;


    public Product(String productId) {
        String productLinkSelector = String.format("[href='#/product/%s']", productId);
        this.link = $(productLinkSelector);
        SelenideElement parentCard = link.parent().parent();
        this.addToCartButton = parentCard.$(".fa-cart-plus");
        this.addToFavoritesButton = parentCard.$(".fa-heart");
        this.removeFromFavoritesButton = parentCard.$(".fa-heart-broken");
        this.description = parentCard.$(".text-center .card-text");
        this.price = parentCard.$(".card-footer span");
        this.image = parentCard.$(".card-img");
        this.fullHeart = parentCard.$(".fa-heart");
        this.brokenHeart = parentCard.$(".fa-heart-broken");
        this.inStockText = parentCard.$(".card-text small");
    }

    @Step("Product name is displayed")
    public String getProductName() {
        return link.getText();
    }

    @Step("Product description is displayed")
    public String getProductDescription() {
        return description.getText();
    }

    @Step("Product price is displayed")
    public String getProductPrice() {
        return price.getText();
    }

    @Step("Product image is displayed")
    public boolean hasImage() {
        return image.has(Condition.image);
    }

    @Step("Add to cart")
    public void addToCart() {
        addToCartButton.click();
    }

    @Step("Add to favorites")
    public void addToFavorites() {
        addToFavoritesButton.click();
    }

    @Step("Remove from favorites")
    public void removeFromFavorites() {
        removeFromFavoritesButton.click();
    }

    @Step("Broken heart is displayed")
    public boolean isBrokenHeartDisplayed() {
        return brokenHeart.isDisplayed();
    }

    @Step("Full heart is displayed")
    public boolean isFullHeartDisplayed() {
        return fullHeart.isDisplayed();
    }

    @Step("Stock status is displayed")
    public String getStockStatus() {
        return inStockText.getText();
    }

}


