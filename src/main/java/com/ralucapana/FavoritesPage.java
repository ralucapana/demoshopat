package com.ralucapana;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class FavoritesPage {

    private final SelenideElement subHeader = $(".subheader-container");

    @Step("Read page title")
    public String getSubHeaderText() {
        return subHeader.getText();
    }
}
