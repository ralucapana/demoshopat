package com.ralucapana;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class ProductInFavorites {


    private final SelenideElement productLink;
    private final SelenideElement brokenHeart;
    private final SelenideElement parentCard;


    public ProductInFavorites(String productId) {
        String productLinkSelector = String.format("[href='#/product/%s']", productId);
        this.productLink = $(productLinkSelector);
        this.parentCard = productLink.parent().parent();
        this.brokenHeart = parentCard.$(".fa-heart-broken");
    }

    @Step("Display product name")
    public String getProductName() {
        return productLink.getText();
    }

    @Step("Remove from Favorites")
    public void removeFromFavorites() {
        brokenHeart.click();
    }

    @Step("Display parent cart")
    public boolean isParentCardDisplayed() {
        return parentCard.isDisplayed();
    }
}
