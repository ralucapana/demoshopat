package com.ralucapana;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class OrderCompletePage {

    private final SelenideElement thankYouMessage = $(".text-center");
    private final SelenideElement shoppingCartBadge = $(".shopping_cart_badge");
    private final SelenideElement continueShoppingButton = $(".btn-success");

    @Step("Display thank you message")
    public String getThankYouMessage() {
        return thankYouMessage.getText();
    }

    @Step("Shopping cart badge is displayed")
    public boolean isShoppingCartBadgeDisplayed() {
        return shoppingCartBadge.isDisplayed();
    }

    @Step("Click on Continue shopping Button")
    public void pressContinueShoppingButton() {
        continueShoppingButton.click();
    }

}
