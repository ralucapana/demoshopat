package com.ralucapana;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class ProductInCart {

    private final SelenideElement increment;
    private final SelenideElement decrement;
    private final SelenideElement quantity;
    private final SelenideElement unitPrice;
    private final SelenideElement totalPrice;
    private final SelenideElement productLink;
    private final SelenideElement deleteButton;


    public ProductInCart(String productId) {
        String productLinkSelector = String.format("#item_%s_title_link", productId);
        this.productLink = $(productLinkSelector);
        SelenideElement parentRow = productLink.parent().parent();
        this.increment = parentRow.$(".fa-plus-circle");
        this.decrement = parentRow.$(".fa-minus-circle");
        this.deleteButton = parentRow.$(".fa-trash");
        ElementsCollection columns = parentRow.$$(".col-md-auto");
        this.quantity = columns.get(0);
        this.unitPrice = columns.get(1);
        this.totalPrice = columns.get(2);
    }

    @Step("Display product name")
    public String getProductName() {
        return productLink.getText();
    }

    @Step("Display product quantity")
    public String getProductCount() {
        return quantity.getText();
    }

    @Step("Display product unitary price")
    public String getProductUnitPrice() {
        return unitPrice.getText();
    }

    @Step("Display product total price")
    public String getProductTotalPrice() {
        return totalPrice.getText();
    }

    @Step("Increase quantity")
    public void increaseQuantity() {
        increment.click();
    }

    @Step("Decrease quantity")
    public void decreaseQuantity() {
        decrement.click();
    }

    @Step("Delete product")
    public void deleteProduct() {
        deleteButton.click();
    }

}
