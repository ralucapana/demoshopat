package com.ralucapana;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ProductListSearchAndSortTest {
    public static final String EXISTING_UNIQUE_SEARCH_WORD = "incredible";
    public static final String EXISTING_UNIQUE_WORD_IN_PRODUCT_NAME = "Incredible";
    public static final String EXISTING_SEARCH_WORD = "wooden";
    public static final String EXISTING_WORD_IN_PRODUCT_NAME = "Wooden";
    public static final String EXISTING_MULTIPLE_WORDS_SEARCH_EXPRESSION = "practical wooden";
    public static final String EXISTING_MULTIPLE_WORDS_IN_PRODUCT_NAME = "Practical Wooden";
    public static final String EXISTING_SEARCH_WORD_CONTAINING_DIFFERENT_CASE_LETTERS = "InCreDible";
    public static final String NON_EXISTING_SEARCH_WORD = "unexisting";
    public static final String REFINED_FROZEN_MOUSE_PRODUCT = "Refined Frozen Mouse";
    public static final String AWESOME_GRANITE_CHIPS_PRODUCT = "Awesome Granite Chips";
    private final MainPage mainPage = new MainPage();
    private final Footer footer = new Footer();


    @BeforeMethod(description = "Before running tests, main page must be open")
    public void setup() {
        mainPage.openPage();
    }

    @AfterMethod(description = "Between tests, the page is being reset")
    public void cleanup() {
        Selenide.refresh();
        footer.resetPage();
    }

    @Test(description = "When opening main page, search field is displayed",
            testName = "Search field is displayed", priority = 1, suiteName = "Search and sort Test")
    public void whenOpeningPage_isSearchFieldDisplayed() {
        assertTrue(mainPage.isSearchFieldDisplayed());
    }

    @Test(description = "When opening main page, search button is displayed",
            testName = "Search button is displayed", priority = 1, suiteName = "Search and sort Test")
    public void whenOpeningPage_isSearchButtonDisplayed() {
        assertTrue(mainPage.isSearchButtonDisplayed());
    }

    @Test(description = "When typing in search field a unique existing word, the correct product is displayed ",
            testName = "Search by unique existing word", priority = 1, suiteName = "Search and sort Test")
    public void whenTypingInSearchFieldUniqueExistingWord_correctProductIsDisplayed() {
        mainPage.typeInSearchField(EXISTING_UNIQUE_SEARCH_WORD);
        mainPage.clickOnTheSearchButton();
        Product product = new Product("2");
        assertTrue(product.getProductName().contains(EXISTING_UNIQUE_WORD_IN_PRODUCT_NAME));
    }

    @Test(description = "When typing in search field an existing word, the correct products are displayed ",
            testName = "Search by existing word", priority = 1, suiteName = "Search and sort Test")
    public void whenTypingInSearchFieldExistingWord_correctProductsAreDisplayed() {
        mainPage.typeInSearchField(EXISTING_SEARCH_WORD);
        mainPage.clickOnTheSearchButton();
        Product product1 = new Product("4");
        Product product2 = new Product("6");
        assertTrue(product1.getProductName().contains(EXISTING_WORD_IN_PRODUCT_NAME));
        assertTrue(product2.getProductName().contains(EXISTING_WORD_IN_PRODUCT_NAME));
    }

    @Test(description = "When typing in search field multiple existing words, the correct products are displayed ",
            testName = "Search by multiple existing words", priority = 1, suiteName = "Search and sort Test")
    public void whenTypingInSearchFieldMultipleExistingWords_correctProductsAreDisplayed() {
        mainPage.typeInSearchField(EXISTING_MULTIPLE_WORDS_SEARCH_EXPRESSION);
        mainPage.clickOnTheSearchButton();
        Product product1 = new Product("4");
        Product product2 = new Product("6");
        assertTrue(product1.getProductName().contains(EXISTING_MULTIPLE_WORDS_IN_PRODUCT_NAME));
        assertTrue(product2.getProductName().contains(EXISTING_MULTIPLE_WORDS_IN_PRODUCT_NAME));
    }

    @Test(description = "When typing in search field an existing word containing upper and lower cases, the correct product is displayed ",
            testName = "Search by existing word containing upper and lower cases", priority = 1, suiteName = "Search and sort Test")
    public void whenTypingInSearchFieldExistingWordWithUpperCasesInIt_correctProductIsDisplayed() {
        mainPage.typeInSearchField(EXISTING_SEARCH_WORD_CONTAINING_DIFFERENT_CASE_LETTERS);
        mainPage.clickOnTheSearchButton();
        Product product = new Product("2");
        assertTrue(product.getProductName().contains(EXISTING_UNIQUE_WORD_IN_PRODUCT_NAME));
    }

    @Test(description = "When typing in search field a non existing word, no products are displayed ",
            testName = "Search by existing word", priority = 1, suiteName = "Search and sort Test")
    public void whenTypingInSearchFieldNonExistingWord_noProductsAreDisplayed() {
        mainPage.typeInSearchField(NON_EXISTING_SEARCH_WORD);
        mainPage.clickOnTheSearchButton();
        assertTrue(mainPage.isProductListPageEmpty());
    }

    @Test(description = "When sorting products from Z to A, the correct order is displayed ",
            testName = "Sort from Z to A", priority = 1, suiteName = "Search and sort Test")
    public void whenSortingTheProductsFromZtoA_theCorrectOrderIsDisplayed() {
        mainPage.sortProducts("za");
        Product product = new Product("0");
        assertEquals(product.getProductName(), REFINED_FROZEN_MOUSE_PRODUCT);
    }

    @Test(description = "When sorting products from A to Z, the correct order is displayed ",
            testName = "Sort from A to Z", priority = 1, suiteName = "Search and sort Test")
    public void whenSortingTheProductsFromAtoZ_theCorrectOrderIsDisplayed() {
        mainPage.sortProducts("az");
        Product product = new Product("1");
        assertEquals(product.getProductName(), AWESOME_GRANITE_CHIPS_PRODUCT);
    }

    @Test(description = "When sorting products from lowest to highest price, the correct order is displayed ",
            testName = "Sort from low to high", priority = 1, suiteName = "Search and sort Test")
    public void whenSortingTheProductsFromLowToHighPrice_theCorrectOrderIsDisplayed() {
        mainPage.sortProducts("lohi");
        Product product = new Product("6");
        assertEquals(product.getProductPrice(), "1.99");
    }

    @Test(description = "When sorting products from highest to lowest price, the correct order is displayed ",
            testName = "Sort from high to low", priority = 1, suiteName = "Search and sort Test")
    public void whenSortingTheProductsFromHighToLowPrice_theCorrectOrderIsDisplayed() {
        mainPage.sortProducts("hilo");
        Product product = new Product("5");
        assertEquals(product.getProductPrice(), "29.99");
    }

}
