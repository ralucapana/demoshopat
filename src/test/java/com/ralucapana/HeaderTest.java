package com.ralucapana;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class HeaderTest {

    public static final String GENERAL_GREETINGS_MESSAGE = "Hello guest!";
    public static final String LOGIN_MODAL_TITLE = "Login";
    private final MainPage mainPage = new MainPage();
    private final Header header = new Header();
    private final Footer footer = new Footer();

    @BeforeMethod(description = "Before running tests, main page must be open")
    public void setup() {
        mainPage.openPage();
    }

    @AfterMethod(description = "Between tests, the page is being reset")
    public void cleanup() {
        Selenide.refresh();
        footer.resetPage();
    }

    @Test(description = "When opening main page and there is no user logged in, a general greeting message is displayed on header",
            testName = "General greetings message is displayed", priority = 2, suiteName = "Header Test")
    public void getGreetingsMessage() {
        assertEquals(header.getGreetingsMessage(), GENERAL_GREETINGS_MESSAGE);
    }

    @Test(description = "When opening main page, there is a Shopping bag icon displayed",
            testName = "Shopping bag icon is displayed", priority = 2, suiteName = "Header Test")
    public void whenOpeningDemoShopPage_pageHasShoppingBagIconDisplayed() {
        assertTrue(header.isShoppingBagIconDisplayed());
    }

    @Test(description = "When opening main page, there is a Shopping cart icon displayed",
            testName = "Shopping cart icon is displayed", priority = 2, suiteName = "Header Test")
    public void whenOpeningDemoShopPage_pageHasShoppingCartIconDisplayed() {
        assertTrue(header.isShoppingCartIconDisplayed());
    }

    @Test(description = "When opening main page, there is a Favorites icon displayed",
            testName = "Favorites icon is displayed", priority = 2, suiteName = "Header Test")
    public void whenOpeningDemoShopPage_pageHasFavoritesIconDisplayed() {
        assertTrue(header.isFavoritesIconDisplayed());
    }

    @Test(description = "On header, clicking on the Login icon opens the login modal",
            testName = "Login icon opens login modal", priority = 2, suiteName = "Header Test")
    public void clickOnLoginIcon() {
        header.clickOnTheLoginIcon();
        LoginModal loginModal = new LoginModal();
        assertEquals(loginModal.getLoginModalTitle(), LOGIN_MODAL_TITLE);

    }
}
