package com.ralucapana;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

public class ProductInCartTest {
    public static final String MAIN_PAGE_TITLE = "Demo shop";
    public static final String AWESOME_GRANITE_CHIPS_PRODUCT = "Awesome Granite Chips";
    public static final String EMPTY_CART_MESSAGE = "How about adding some products in your cart?";
    private final MainPage mainPage = new MainPage();
    private final Header header = new Header();
    private final Footer footer = new Footer();
    private final CartPage cartPage = new CartPage();
    private final ProductInCart productInCart = new ProductInCart("1");
    private final Product p1 = new Product("1");

    @BeforeMethod(description = "Before running tests, main page must be open")
    public void setup() {
        mainPage.openPage();
    }

    @AfterMethod(description = "Between tests, the page is being reset")
    public void cleanup() {
        Selenide.refresh();
        footer.resetPage();
    }

    @Test(description = "When using Add to cart button from product, the product is added to Shopping cart",
            testName = "Add product to cart", priority = 1, suiteName = "Product in cart Test")
    public void whenAddingAProductToCart_productIsAddedToCart() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(productInCart.getProductName(), AWESOME_GRANITE_CHIPS_PRODUCT);
    }

    @Test(description = "When adding to cart a product, tha cart quantity is increased by one",
            testName = "Cart quantity is increased", priority = 1, suiteName = "Product in cart Test")
    public void whenAddingAProductToCart_productInCartHasQuantityOne() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(productInCart.getProductCount(), "1");
    }

    @Test(description = "When adding product to cart, product in cart has unit price displayed",
            testName = "Unit Price is displayed for product in cart", priority = 1, suiteName = "Product in cart Test")
    public void whenAddingAProductToCart_productInCartHasPrice() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(productInCart.getProductUnitPrice(), "$15.99");
    }

    @Test(description = "When adding product to cart, product in cart has total product price displayed",
            testName = "Total product price is displayed for product in cart", priority = 1, suiteName = "Product in cart Test")
    public void whenAddingAProductToCart_productInCartHasTotalPrice() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(productInCart.getProductTotalPrice(), "$15.99");
    }

    @Test(description = "When clicking on the incrementing button from Products in cart, the product quantity is increased accordingly",
            testName = "Increase quantity in cart", priority = 1, suiteName = "Product in cart Test")
    public void whenClickingOnceOnPlusInProductInCart_productInCartQuantityIsIncreasedByOne() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(productInCart.getProductCount(), "1");
        productInCart.increaseQuantity();
        assertEquals(productInCart.getProductCount(), "2");
    }

    @Test(description = "When clicking on the decrementing button from Products in cart, the product quantity is decreased accordingly",
            testName = "Decrease quantity in cart", priority = 1, suiteName = "Product in cart Test")
    public void whenClickingOnceOnMinusInProductInCart_productInCartQuantityIsDecreasedByOne() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        productInCart.increaseQuantity();
        assertEquals(productInCart.getProductCount(), "2");
        productInCart.decreaseQuantity();
        assertEquals(productInCart.getProductCount(), "1");
    }

    @Test(description = "When clicking on the decrementing button from Products in cart, when there is only one product in cart, product is deleted",
            testName = "Delete product in Product in cart", priority = 1, suiteName = "Product in cart Test")
    public void whenClickingOnMinusInProductInCart_whenThereIsOnlyOneProductInCart_productInCartIsDeleted() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(productInCart.getProductCount(), "1");
        productInCart.decreaseQuantity();
        assertEquals(cartPage.getWelcomingMessage(), EMPTY_CART_MESSAGE);
    }

    @Test(description = "When clicking on the delete button, product is deleted, regardless of quantity",
            testName = "Delete product using Delete button", priority = 1, suiteName = "Product in cart Test")
    public void whenClickingOnDeleteButton_productInCartIsDeleted() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(productInCart.getProductCount(), "1");
        productInCart.deleteProduct();
        assertEquals(cartPage.getWelcomingMessage(), EMPTY_CART_MESSAGE);
    }

    @Test(description = "When deleting the only product from cart, Shopping cart badge disappears",
            testName = "Shopping cart badge disappears when cart is empty", priority = 2, suiteName = "Product in cart Test")
    public void whenDeletingTheProductFromCart_shoppingCartBadgeDisappears() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(header.getCartCounter(), "1");
        productInCart.deleteProduct();
        assertFalse(header.isShoppingCartIconBadgeDisplayed());
    }

    @Test(description = "When deleting product from cart, Shopping cart badge is updated accordingly",
            testName = "Shopping cart badge updates when product is deleted", priority = 2, suiteName = "Product in cart Test")
    public void whenDeletingAProductFromCart_shoppingCartBadgeIsUpdatedAccordingly() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        productInCart.increaseQuantity();
        assertEquals(header.getCartCounter(), "2");
        productInCart.decreaseQuantity();
        assertEquals(header.getCartCounter(), "1");
    }

    @Test(description = "When increasing the quantity of product in cart, Shopping cart badge is updated accordingly",
            testName = "Shopping cart badge updates when product quantity increases", priority = 2, suiteName = "Product in cart Test")
    public void whenIncreasingTheQuantityOfProductInCart_productInCartTotalPriceIsIncreasedAccordingly() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(productInCart.getProductCount(), "1");
        productInCart.increaseQuantity();
        assertEquals(productInCart.getProductCount(), "2");
        assertEquals(productInCart.getProductTotalPrice(), "$31.98");
    }

    @Test(description = "When pressing the About version link when in shopping cart page, user is taken to product list page",
            testName = "About version takes user to product list page", priority = 2, suiteName = "Product in cart Test")
    public void whenPressingAboutVersionLink_withProductInCart_productListPageIsDisplayed() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(productInCart.getProductCount(), "1");
        footer.pressAboutVersionLink();
        assertEquals(mainPage.getPageTitle(), MAIN_PAGE_TITLE);
    }




}
