package com.ralucapana;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Issue;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

public class FavoritesPageTest {
    public static final String PAGE_TITLE = "Wishlist";
    private final MainPage mainPage = new MainPage();
    private final Header header = new Header();
    private final Footer footer = new Footer();
    private final FavoritesPage favoritesPage = new FavoritesPage();
    private final LoginModal loginModal = new LoginModal();
    private final Product p1 = new Product("1");
    public static final String VALID_USERNAME = "dino";
    public static final String VALID_PASSWORD = "choochoo";
    public static final String GREETINGS_MESSAGE = "Hi dino!";
    public static final String GENERAL_GREETINGS_MESSAGE = "Hello guest!";


    @BeforeMethod(description = "Before running tests, main page must be open")
    public void setup() {
        mainPage.openPage();
    }

    @AfterMethod(description = "Between tests, the page is being reset")
    public void cleanup() {
        Selenide.refresh();
        footer.resetPage();
    }

    @Test(description = "When the favorites page is open, page title is displayed", testName = "Open favorites page",
            priority = 1, suiteName = "Favorites Page")
    public void whenFavoritesPageIsOpen_WishlistTitleIsDisplayed() {
        header.clickOnTheFavoritesButton();
        assertEquals(favoritesPage.getSubHeaderText(), PAGE_TITLE);

    }

    @Issue("Favorites Badge Error")
    @Severity(SeverityLevel.NORMAL)
    @Test(description = "When there is a product added to favorites and the user logs out, the favorites icon badge disappears accordingly",
            testName = "Favorites icon badge disappears when the user logs out", priority = 2,
            suiteName = "Favorites Page Test")
    public void whenAUserLogsOut_shoppingCartBadgeDisappearsAccordingly() {
        header.clickOnTheLoginIcon();
        loginModal.typeInUsername(VALID_USERNAME);
        loginModal.typeInPassword(VALID_PASSWORD);
        loginModal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), GREETINGS_MESSAGE);
        p1.addToFavorites();
        header.clickOnTheShoppingCartButton();
        assertEquals(header.getFavoritesCounter(), "1");
        header.clickOnTheLogoutButton();
        assertEquals(header.getGreetingsMessage(), GENERAL_GREETINGS_MESSAGE);
        assertFalse(header.isFavoritesIconBadgeDisplayed());
    }


}
