package com.ralucapana;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Issue;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CartPageTest {

    public static final String AWESOME_GRANITE_CHIPS_PRODUCT = "Awesome Granite Chips";
    public static final String AWESOME_METAL_CHAIR_PRODUCT = "Awesome Metal Chair";
    public static final String SUBHEADER_TEXT_PRODUCTS = "Products";
    public static final String SUBHEADER_TEXT_INFORMATION = "Your information";
    public static final String EMPTY_CART_MESSAGE = "How about adding some products in your cart?";
    private final MainPage mainPage = new MainPage();
    private final CartPage cartPage = new CartPage();
    private final Header header = new Header();
    private final Footer footer = new Footer();
    private final LoginModal loginModal = new LoginModal();
    private final Product p1 = new Product("1");
    private final Product p2 = new Product("3");
    private final ProductInCart productInCart = new ProductInCart("1");
    public static final String VALID_USERNAME = "dino";
    public static final String VALID_PASSWORD = "choochoo";
    public static final String GREETINGS_MESSAGE = "Hi dino!";
    public static final String GENERAL_GREETINGS_MESSAGE = "Hello guest!";

    @BeforeMethod(description = "Before running tests, main page must be open")
    public void setup() {
        mainPage.openPage();
    }

    @AfterMethod(description = "Between tests, the page is being reset")
    public void cleanup() {
        Selenide.refresh();
        footer.resetPage();
    }

    @Test(description = "When cart page is empty, a message about adding products to the cart is displayed",
            testName = "Cart Page Is Empty", priority = 2, suiteName = "Cart Page Tests")
    public void whenCartPageIsEmpty_messageIsDisplayed() {
        header.clickOnTheShoppingCartButton();
        assertEquals(cartPage.getWelcomingMessage(), EMPTY_CART_MESSAGE);
    }

    @Test(description = "When there are products in the cart, items total is displayed",
            testName = "Cart with products has items total displayed", priority = 2, suiteName = "Cart Page Tests")
    public void whenOpeningPageCart_andThereIsProductInCart_itemsTotalIsDisplayed() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertTrue(cartPage.isItemsTotalDisplayed());
    }

    @Test(description = "When there are products in the cart, tax is displayed",
            testName = "Cart with products has tax displayed", priority = 2, suiteName = "Cart Page Tests")
    public void whenOpeningPageCart_andThereIsProductInCart_taxIsDisplayed() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertTrue(cartPage.isTaxDisplayed());
    }

    @Test(description = "When there are products in the cart, total amount is displayed",
            testName = "Cart with products has total amount displayed", priority = 2, suiteName = "Cart Page Tests")
    public void whenOpeningPageCart_andThereIsProductInCart_totalAmountIsDisplayed() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertTrue(cartPage.isTotalAmountDisplayed());
    }

    @Test(description = "When a product is added to cart, items total price is generated",
            testName = "Product added to cart generates items total", priority = 2, suiteName = "Cart Page Tests")
    public void whenProductIsAddedToCart_itemsTotalIsDisplayed() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(header.getCartCounter(), "1");
        assertEquals(cartPage.getItemsTotal(), "$15.99");
    }

    @Test(description = "When a product is added to cart, tax is generated",
            testName = "Product added to cart generates tax", priority = 2, suiteName = "Cart Page Tests")
    public void whenProductIsAddedToCart_taxIsDisplayed() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(header.getCartCounter(), "1");
        assertEquals(cartPage.getTax(), "$0.80");
    }

    @Test(description = "When a product is added to cart, total amount price is generated",
            testName = "Product added to cart generates total amount", priority = 2, suiteName = "Cart Page Tests")
    public void whenProductIsAddedToCart_amountTotalIsDisplayed() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(header.getCartCounter(), "1");
        assertEquals(cartPage.getTotalAmount(), "$15.99");
    }

    @Test(description = "When product in cart quantity is increased, items total price is updated accordingly",
            testName = "Product in cart's quantity change updates items total price", priority = 2, suiteName = "Cart Page Tests")
    public void whenProductInCartQuantityIsIncreased_itemsTotalPriceIsIncreasedAccordingly() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        productInCart.increaseQuantity();
        assertEquals(cartPage.getItemsTotal(), "$31.98");
    }

    @Test(description = "When product in cart quantity is increased, tax value is updated accordingly",
            testName = "Product in cart's quantity change updates tax value", priority = 2, suiteName = "Cart Page Tests")
    public void whenProductInCartQuantityIsIncreased_taxIsIncreasedAccordingly() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        productInCart.increaseQuantity();
        assertEquals(cartPage.getTax(), "$1.60");
    }

    @Test(description = "When product in cart quantity is increased, total amount value is updated accordingly",
            testName = "Product in cart's quantity change updates total amount value", priority = 2, suiteName = "Cart Page Tests")
    public void whenProductInCartQuantityIsIncreased_totalAmountPriceIsIncreasedAccordingly() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        productInCart.increaseQuantity();
        assertEquals(cartPage.getTotalAmount(), "$31.98");
    }

    @Test(description = "When there are different products added to the cart, they are all displayed",
            testName = "All products added to the cart are displayed", priority = 2, suiteName = "Cart Page Tests")
    public void whenTwoDifferentProductsAreAddedToTheCart_bothProductsAreDisplayedInCartPage() {
        p1.addToCart();
        p2.addToCart();
        header.clickOnTheShoppingCartButton();
        ProductInCart productInCart1 = new ProductInCart("1");
        ProductInCart productInCart2 = new ProductInCart("3");
        assertEquals(productInCart1.getProductName(), AWESOME_GRANITE_CHIPS_PRODUCT);
        assertEquals(productInCart2.getProductName(), AWESOME_METAL_CHAIR_PRODUCT);
    }

    @Test(description = "When there are products in cart and Continue shopping button is pressed, user is taken to products list page",
            testName = "In cart page Continue shopping button opens the product list page", priority = 2,
            suiteName = "Cart Page Test")
    public void whenCartPageHasProducts_andContinueShoppingButtonIsPressed_productPageIsDisplayed() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(header.getCartCounter(), "1");
        cartPage.continueShopping();
        assertEquals(mainPage.getSubHeaderText(), SUBHEADER_TEXT_PRODUCTS);
    }

    @Test(description = "When there are products in cart and Check out button is pressed, user is taken to check out page",
            testName = "In cart page Check out button opens the checkout page", priority = 2,
            suiteName = "Cart Page Test")
    public void whenCartPageHasProducts_andCheckOutButtonIsPressed_checkOutPageIsDisplayed() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(header.getCartCounter(), "1");
        cartPage.checkOut();
        assertEquals(mainPage.getSubHeaderText(), SUBHEADER_TEXT_INFORMATION);
    }

    @Test(description = "When cart is empty and Shopping bag icon is pressed, user is taken to products list page",
            testName = "In empty cart page Shopping bag icon opens the products list page", priority = 2,
            suiteName = "Cart Page Test")
    public void whenInEmptyCartPage_andShoppingBagIconIsPressed_productPageIsDisplayed() {
        header.clickOnTheShoppingCartButton();
        assertEquals(cartPage.getWelcomingMessage(), EMPTY_CART_MESSAGE);
        header.clickOnTheShoppingBagIcon();
        assertEquals(mainPage.getSubHeaderText(), SUBHEADER_TEXT_PRODUCTS);
    }

    @Test(description = "When there are products in cart and Shopping bag icon is pressed, user is taken to product list page",
            testName = "In cart page with products, Shopping bag icon opens the products list page", priority = 2,
            suiteName = "Cart Page Test")
    public void whenCartPageHasProducts_andShoppingBagIconIsPressed_productPageIsDisplayed() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(header.getCartCounter(), "1");
        header.clickOnTheShoppingBagIcon();
        assertEquals(mainPage.getSubHeaderText(), SUBHEADER_TEXT_PRODUCTS);
    }

    @Test(description = "When cart is empty and About version link is pressed, user is taken to products list page",
            testName = "In empty cart page About version link opens the products list page page", priority = 2,
            suiteName = "Cart Page Test")
    public void whenInEmptyCartPage_andAboutVersionLinkIsPressed_productPageIsDisplayed() {
        header.clickOnTheShoppingCartButton();
        assertEquals(cartPage.getWelcomingMessage(), EMPTY_CART_MESSAGE);
        footer.pressAboutVersionLink();
        assertEquals(mainPage.getSubHeaderText(), SUBHEADER_TEXT_PRODUCTS);
    }

    @Test(description = "When cart has product and About version link is pressed, user is taken to products list page",
            testName = "In cart with product page About version link opens the products list page page", priority = 2,
            suiteName = "Cart Page Test")
    public void whenCartPageHasProducts_andAboutVersionLinkIsPressed_productPageIsDisplayed() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(header.getCartCounter(), "1");
        footer.pressAboutVersionLink();
        assertEquals(mainPage.getSubHeaderText(), SUBHEADER_TEXT_PRODUCTS);
    }

    @Test(description = "When cart has product and reset button is pressed, cart page is emptied",
            testName = "Reset button empties cart page with product", priority = 2,
            suiteName = "Cart Page Test")
    public void whenCartPageHasProduct_andResetButtonIsPressed_CartPageIsEmptied() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(header.getCartCounter(), "1");
        footer.pressCancelButton();
        assertEquals(cartPage.getWelcomingMessage(), EMPTY_CART_MESSAGE);
    }

    @Issue("Shopping Cart Badge Error")
    @Severity(SeverityLevel.NORMAL)
    @Test(description = "When there is a product in the shopping cart and the user logs out, the shopping cart is emptied accordingly",
            testName = "Cart badge disappears when the user logs out", priority = 2,
            suiteName = "Cart Page Test")
    public void whenAUserLogsOut_shoppingCartBadgeDisappearsAccordingly() {
        header.clickOnTheLoginIcon();
        loginModal.typeInUsername(VALID_USERNAME);
        loginModal.typeInPassword(VALID_PASSWORD);
        loginModal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), GREETINGS_MESSAGE);
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertEquals(header.getCartCounter(), "1");
        header.clickOnTheLogoutButton();
        assertEquals(header.getGreetingsMessage(), GENERAL_GREETINGS_MESSAGE);
        assertFalse(header.isShoppingCartIconBadgeDisplayed());
    }

}
