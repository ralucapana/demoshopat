package com.ralucapana;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class LogoutTest {

    public static final String VALID_USERNAME = "dino";
    public static final String VALID_PASSWORD = "choochoo";
    public static final String GREETINGS_MESSAGE = "Hi dino!";
    public static final String GENERAL_GREETINGS_MESSAGE = "Hello guest!";


    @Test(description = "When a user that has been logged in, clicks on the Logout button, the account is logged out successfully",
            testName = "Successful logout", priority = 1, suiteName = "Logout Test")
    public void whenALoggedInUser_clicksOnTheLogoutButton_logoutIsSuccessful() {
        MainPage mainPage = new MainPage();
        mainPage.openPage();
        Header header = new Header();
        header.clickOnTheLoginIcon();
        LoginModal loginModal = new LoginModal();
        loginModal.typeInUsername(VALID_USERNAME);
        loginModal.typeInPassword(VALID_PASSWORD);
        loginModal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), GREETINGS_MESSAGE);
        header.clickOnTheLogoutButton();
        assertEquals(header.getGreetingsMessage(), GENERAL_GREETINGS_MESSAGE);
    }
}
