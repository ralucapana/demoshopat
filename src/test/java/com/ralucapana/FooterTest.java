package com.ralucapana;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class FooterTest {
    private final MainPage mainPage = new MainPage();
    private final Footer footer = new Footer();

    @BeforeMethod(description = "Before running tests, main page must be open")
    public void setup() {
        mainPage.openPage();
    }

    @AfterMethod(description = "Between tests, the page is being reset")
    public void cleanup() {
        Selenide.refresh();
        footer.resetPage();
    }

    @Test(description = "When opening main page, reset button is displayed on footer", testName = "Reset button is displayed",
            priority = 2, suiteName = "Footer Test")
    public void WhenOpeningThePage_resetPageButtonIsDisplayed() {
        assertTrue(footer.isResetPageDisplayed());
    }

    @Test(description = "When opening main page, About version link is displayed on footer", testName = "About version link is displayed",
            priority = 2, suiteName = "Footer Test")
    public void whenOpeningThePage_aboutVersionIsDisplayed() {
        assertTrue(footer.isAboutVersionDisplayed());
    }

    @Test(description = "When opening main page, User modal button is displayed on footer", testName = "User modal button is displayed",
            priority = 2, suiteName = "Footer Test")
    public void whenOpeningThePage_userModalButtonIsDisplayed() {
        assertTrue(footer.isUserModalButtonDisplayed());
    }
}