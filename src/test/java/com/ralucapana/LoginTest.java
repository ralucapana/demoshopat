package com.ralucapana;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class LoginTest {
    public static final String VALID_USERNAME = "dino";
    public static final String VALID_PASSWORD = "choochoo";
    public static final String INVALID_USERNAME = "user";
    public static final String INVALID_PASSWORD = "password";
    public static final String VALID_USERNAME_WITH_INCORRECT_LETTERCASE = "Dino";
    public static final String VALID_PASSWORD_WITH_INCORRECT_LETTERCASE = "Choochoo";
    public static final String VALID_USERNAME_WITH_INCORRECT_SPECIAL_CHARACTER = "din@";
    public static final String VALID_PASSWORD_WITH_INCORRECT_SPECIAL_CHARACTER = "ch@ochoo";
    public static final String LOCKED_USER = "locked";
    public static final String GREETINGS_MESSAGE = "Hi dino!";
    public static final String INCORRECT_USERNAME_OR_PASSWORD_MESSAGE = "Incorrect username or password!";
    public static final String MISSING_PASSWORD_MESSAGE = "Please fill in the password!";
    public static final String MISSING_USERNAME_MESSAGE = "Please fill in the username!";
    public static final String LOCKED_OUT_MESSAGE = "The user has been locked out.";
    private final MainPage mainPage = new MainPage();
    private final Header header = new Header();
    private final LoginModal loginModal = new LoginModal();
    private final Footer footer = new Footer();

    @BeforeMethod(description = "Before running tests, main page and login modal must be open")
    public void setup() {
        mainPage.openPage();
        header.clickOnTheLoginIcon();
    }

    @AfterMethod(description = "Between tests, the page is being reset")
    public void cleanup() {
        Selenide.refresh();
        footer.resetPage();
    }

    @Test(description = "When logging in with valid credentials, login is successful",
            testName = "Login with valid credentials", priority = 1, suiteName = "Login")
    public void whenLoggingIn_withValidCredentials_loginIsSuccessful() {
        loginModal.typeInUsername(VALID_USERNAME);
        loginModal.typeInPassword(VALID_PASSWORD);
        loginModal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), GREETINGS_MESSAGE);
    }

    @Test(description = "When logging in with invalid username, warning message about incorrect username or password is generated",
            testName = "Login with invalid username", priority = 1, suiteName = "Login")
    public void whenLoggingIn_withInvalidUsername_warningMessageAppears() {
        loginModal.typeInUsername(INVALID_USERNAME);
        loginModal.typeInPassword(VALID_PASSWORD);
        loginModal.clickOnTheLoginButton();
        assertEquals(loginModal.getWarningMessage(), INCORRECT_USERNAME_OR_PASSWORD_MESSAGE);
    }

    @Test(description = "When logging in with invalid password, warning message about incorrect username or password is generated",
            testName = "Login with invalid password", priority = 1, suiteName = "Login")
    public void whenLoggingIn_withInvalidPassword_warningMessageAppears() {
        loginModal.typeInUsername(VALID_USERNAME);
        loginModal.typeInPassword(INVALID_PASSWORD);
        loginModal.clickOnTheLoginButton();
        assertEquals(loginModal.getWarningMessage(), INCORRECT_USERNAME_OR_PASSWORD_MESSAGE);
    }

    @Test(description = "When logging in with valid username containing incorrect letter case, warning message about incorrect username or password is generated",
            testName = "Login with valid username containing incorrect letter case", priority = 1, suiteName = "Login")
    public void whenLoggingIn_withExistingUserAndIncorrectLetterCase_warningMessageAppears() {
        loginModal.typeInUsername(VALID_USERNAME_WITH_INCORRECT_LETTERCASE);
        loginModal.typeInPassword(VALID_PASSWORD);
        loginModal.clickOnTheLoginButton();
        assertEquals(loginModal.getWarningMessage(), INCORRECT_USERNAME_OR_PASSWORD_MESSAGE);
    }

    @Test(description = "When logging in with valid password containing incorrect letter case, warning message about incorrect username or password is generated",
            testName = "Login with valid password containing incorrect letter case", priority = 1, suiteName = "Login")
    public void whenLoggingIn_withExistingPasswordWithIncorrectLetterCase_warningMessageAppears() {
        loginModal.typeInUsername(VALID_USERNAME);
        loginModal.typeInPassword(VALID_PASSWORD_WITH_INCORRECT_LETTERCASE);
        loginModal.clickOnTheLoginButton();
        assertEquals(loginModal.getWarningMessage(), INCORRECT_USERNAME_OR_PASSWORD_MESSAGE);
    }

    @Test(description = "When logging in with valid username containing incorrect special character, warning message about incorrect username or password is generated",
            testName = "Login with valid username containing incorrect special character", priority = 1,
            suiteName = "Login")
    public void whenLoggingIn_withExistingUsernameContainingSpecialCharacter_warningMessageAppears() {
        loginModal.typeInUsername(VALID_USERNAME_WITH_INCORRECT_SPECIAL_CHARACTER);
        loginModal.typeInPassword(VALID_PASSWORD);
        loginModal.clickOnTheLoginButton();
        assertEquals(loginModal.getWarningMessage(), INCORRECT_USERNAME_OR_PASSWORD_MESSAGE);
    }

    @Test(description = "When logging in with valid password containing incorrect special character, warning message about incorrect username or password is generated",
            testName = "Login with valid password containing incorrect special character", priority = 1,
            suiteName = "Login")
    public void whenLoggingIn_withExistingPasswordContainingSpecialCharacter_warningMessageAppears() {
        loginModal.typeInUsername(VALID_USERNAME);
        loginModal.typeInPassword(VALID_PASSWORD_WITH_INCORRECT_SPECIAL_CHARACTER);
        loginModal.clickOnTheLoginButton();
        assertEquals(loginModal.getWarningMessage(), INCORRECT_USERNAME_OR_PASSWORD_MESSAGE);
    }


    @Test(description = "When logging in without username, a missing username message is generated",
            testName = "Login without username", priority = 1, suiteName = "Login")
    public void whenLoggingIn_withoutUsername_instructionMessageAppears() {
        loginModal.typeInPassword(VALID_PASSWORD);
        loginModal.clickOnTheLoginButton();
        assertEquals(loginModal.getInstructionMessage(), MISSING_USERNAME_MESSAGE);
    }

    @Test(description = "When logging in without password, a missing password message is generated",
            testName = "Login without password", priority = 1, suiteName = "Login")
    public void whenLoggingIn_withoutPassword_instructionMessageAppears() {
        loginModal.typeInUsername(VALID_USERNAME);
        loginModal.clickOnTheLoginButton();
        assertEquals(loginModal.getInstructionMessage(), MISSING_PASSWORD_MESSAGE);
    }

    @Test(description = "When logging in without without credentials, a missing username message is generated",
            testName = "Login without credentials", priority = 1, suiteName = "Login")
    public void whenLoggingIn_withoutCredentials_instructionMessageAppears() {
        loginModal.clickOnTheLoginButton();
        assertEquals(loginModal.getInstructionMessage(), MISSING_USERNAME_MESSAGE);
    }

    @Test(description = "When logging in with locked user, a message about the user being locked out is generated",
            testName = "Login with locked user", priority = 1, suiteName = "Login")
    public void whenLoggingIn_withLockedUser_attentionMessageAppears() {
        loginModal.typeInUsername(LOCKED_USER);
        loginModal.typeInPassword(VALID_PASSWORD);
        loginModal.clickOnTheLoginButton();
        assertEquals(loginModal.getAttentionMessage(), LOCKED_OUT_MESSAGE);
    }
}

