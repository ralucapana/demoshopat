package com.ralucapana;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ProductTest {
    public static final String AWESOME_GRANITE_CHIPS_PRODUCT = "Awesome Granite Chips";
    public static final String PRODUCT_DESCRIPTION = "Omnis excepturi laudantium et minima dignissimos e";
    public static final String PRODUCT_PRICE = "15.99";
    public static final String STOCK_MESSAGE = "in stock";
    private final MainPage mainPage = new MainPage();
    private final Header header = new Header();

    private final Footer footer = new Footer();
    private final Product p1 = new Product("1");
    private final Product p2 = new Product("2");

    @BeforeMethod(description = "Before running tests, main page must be open")
    public void setup() {
        mainPage.openPage();
    }

    @AfterMethod(description = "Between tests, the page is being reset")
    public void cleanup() {
        Selenide.refresh();
        footer.resetPage();
    }


    @Test(description = "When opening page, product has a name", testName = "Product has name",
            priority = 2, suiteName = "Product test")
    public void whenOpeningProductsListPage_productHasName() {
        p1.getProductName();
        assertEquals(p1.getProductName(), AWESOME_GRANITE_CHIPS_PRODUCT);
    }

    @Test(description = "When opening page, product has a description", testName = "Product has description",
            priority = 2, suiteName = "Product test")
    public void whenOpeningProductsListPage_productHasDescription() {
        p1.getProductDescription();
        assertEquals(p1.getProductDescription(), PRODUCT_DESCRIPTION);
    }

    @Test(description = "When opening page, product has a price", testName = "Product has price",
            priority = 2, suiteName = "Product test")
    public void whenOpeningProductsListPage_productHasPrice() {
        p1.getProductPrice();
        assertEquals(p1.getProductPrice(), PRODUCT_PRICE);
    }

    @Test(description = "When opening page, product has an image", testName = "Product has image",
            priority = 2, suiteName = "Product test")
    public void whenOpeningProductsListPage_productHasImage() {
        p1.hasImage();
        assertTrue(p1.hasImage());
    }

    @Test(description = "When adding a product to cart, the Shopping cart badge is increased by one",
            testName = "Shopping cart badge is increased when adding a product to cart",
            priority = 2, suiteName = "Product test")
    public void whenAddingAProductToTheShoppingCart_shoppingCartBadgeIsIncreasedBy1() {
        p1.addToCart();
        header.getCartCounter();
        assertEquals(header.getCartCounter(), "1");
    }

    @Test(description = "When adding a product to Favorites, the Favorites cart badge is increased by one",
            testName = "Favorites cart badge is increased when adding a product to favorites",
            priority = 2, suiteName = "Product test")
    public void whenAddingAProductToFavorites_favoritesIconBadgeIsIncreasedBy1() {
        p1.addToFavorites();
        header.getFavoritesCounter();
        assertEquals(header.getFavoritesCounter(), "1");
    }

    @Test(description = "When adding a product to Favorites, the heart image changes, displaying a broken heart",
            testName = "Heart image changes when adding product to Favorites",
            priority = 2, suiteName = "Product test")
    public void whenAddingAProductToFavorites_heartImageChanges() {
        assertTrue(p1.isFullHeartDisplayed());
        assertFalse(p1.isBrokenHeartDisplayed());
        p1.addToFavorites();
        assertTrue(p1.isBrokenHeartDisplayed());
        assertFalse(p1.isFullHeartDisplayed());
    }

    @Test(description = "When removing a product from Favorites, the Favorites cart badge is decreased by one",
            testName = "Favorites cart badge is decreased when removing a product from favorites",
            priority = 2, suiteName = "Product test")
    public void whenRemovingAProductFromFavorites_favoritesIconBadgeIsDecreasedBy1() {
        p1.addToFavorites();
        p2.addToFavorites();
        header.getFavoritesCounter();
        assertEquals(header.getFavoritesCounter(), "2");
        p2.removeFromFavorites();
        header.getFavoritesCounter();
        assertEquals(header.getFavoritesCounter(), "1");
    }

    @Test(description = "When removing a product from Favorites, the heart image changes, displaying a full heart",
            testName = "Heart image changes when removing product from Favorites",
            priority = 2, suiteName = "Product test")
    public void whenRemovingAProductFromFavorites_heartImageChanges() {
        p1.addToFavorites();
        assertTrue(p1.isBrokenHeartDisplayed());
        assertFalse(p1.isFullHeartDisplayed());
        p1.removeFromFavorites();
        assertTrue(p1.isFullHeartDisplayed());
        assertFalse(p1.isBrokenHeartDisplayed());
    }

    @Test(description = "When opening the page, product displays stock status", testName = "Product displays stock status",
            priority = 2, suiteName = "Product test")
    public void whenOpeningProductsListPage_productDisplaysStockStatus() {
        p1.getStockStatus();
        assertEquals(p1.getStockStatus(), STOCK_MESSAGE);
    }
}

