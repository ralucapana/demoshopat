package com.ralucapana;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MainPageTest {

    public static final String MAIN_PAGE_TITLE = "Demo shop";

    @Test(description = "Web-shop application is accessible when main page is opened", testName = "Open Main page",
    priority = 1, suiteName = "Main Page Test")
    public void openDemoShopPage() {
        MainPage mainPage = new MainPage();
        mainPage.openPage();
        assertEquals(mainPage.getPageTitle(), MAIN_PAGE_TITLE);
    }


}
