package com.ralucapana;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.ralucapana.OrderSummaryPage.ORDER_INFO;
import static org.testng.Assert.*;

//@Ignore
public class OrderSummaryPageTest {

    public static final String FIRST_NAME = "Dino";
    public static final String LAST_NAME = "Dinescu";
    public static final String ADDRESS = "1st Linden St.";
    public static final String SUBHEADER_TEXT_SUMMARY = "Order summary";
    public static final String THANK_YOU_MESSAGE = "Thank you for your order!";
    public static final String SUBHEADER_TEXT_CART = "Your cart";
    private final MainPage mainPage = new MainPage();
    private final Header header = new Header();
    private final CartPage cartPage = new CartPage();
    private final CheckOutPage checkOutPage = new CheckOutPage();
    private final OrderSummaryPage orderSummaryPage = new OrderSummaryPage();
    private final Footer footer = new Footer();
    private final Product p1 = new Product("1");
    private final OrderCompletePage orderCompletePage = new OrderCompletePage();

    @BeforeMethod(description = "Before running tests, product must be added to cart and delivery details filled in")
    public void setup() {
        mainPage.openPage();
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        cartPage.checkOut();
        checkOutPage.fillInFirstName(FIRST_NAME);
        checkOutPage.fillInLastName(LAST_NAME);
        checkOutPage.fillInAddress(ADDRESS);
        checkOutPage.clickOnContinueCheckOutButton();
    }

    @AfterMethod(description = "Between tests, page must be reset")
    public void cleanup() {
        Selenide.refresh();
        footer.resetPage();
    }

    @Test(description = "When opening the page, the title is displayed",
            testName = "Open Order summary page", priority = 1, suiteName = "Order summary test")
    public void whenOrderSummaryPageIsOpen_titleIsDisplayed() {
        assertEquals(mainPage.getSubHeaderText(), SUBHEADER_TEXT_SUMMARY);
    }

    @Test(description = "When opening the Order summary page, there is a Complete your order button displayed",
            testName = "Complete your order button is displayed", priority = 1, suiteName = "Order summary test")
    public void whenOrderSummaryPageIsOpen_completeYourOrderButtonIsDisplayed() {
        assertTrue(orderSummaryPage.isCompleteYourOrderButtonDisplayed());
    }

    @Test(description = "When opening the Order summary page, there is a Cancel button displayed",
            testName = "Cancel button is displayed", priority = 1, suiteName = "Order summary test")
    public void whenOrderSummaryPageIsOpen_cancelButtonIsDisplayed() {
        assertTrue(orderSummaryPage.isCancelButtonDisplayed());
    }

    @Test(description = "On the Order summary page, items total price is displayed",
            testName = "Items total is displayed", priority = 1, suiteName = "Order summary test")
    public void whenOrderSummaryPageIsOpen_itemsTotalPriceIsDisplayed() {
        assertTrue(orderSummaryPage.isItemsTotalDisplayed());
    }

    @Test(description = "On the Order summary page, tax is displayed",
            testName = "Tax is displayed", priority = 1, suiteName = "Order summary test")
    public void whenOrderSummaryPageIsOpen_taxValueIsDisplayed() {
        assertTrue(orderSummaryPage.isTaxDisplayed());
    }

    @Test(description = "On the Order summary page, total amount value is displayed",
            testName = "Total amount is displayed", priority = 1, suiteName = "Order summary test")
    public void whenOrderSummaryPageIsOpen_totalAmountValueIsDisplayed() {
        assertTrue(orderSummaryPage.isTotalAmountDisplayed());
    }


    @Test(description = "On the Order summary page, payment and delivery information is displayed",
            testName = "Payment and delivery information is displayed", priority = 1, suiteName = "Order summary test")
    public void whenOrderSummaryPageIsOpen_paymentInformationIsDisplayed() {
        assertEquals(orderSummaryPage.getPaymentAndDeliveryInformation(), ORDER_INFO);
    }

    @Test(description = "On the Order summary page, when Complete your order button is pressed, a thank you message is displayed",
            testName = "Thank you message is displayed", priority = 1, suiteName = "Order summary test")
    public void whenCompleteYourOrderButtonIsPressed_thankYouMessageIsDisplayed() {
        orderSummaryPage.completeYourOrder();
        assertEquals(orderCompletePage.getThankYouMessage(), THANK_YOU_MESSAGE);
    }

    @Test(description = "On the Order summary page, when Complete your order button is pressed, the order is finalized, shopping cart is emptied" +
            "and shopping cart badge disappears",
            testName = "Complete your order empties shopping cart", priority = 1, suiteName = "Order summary test")
    public void whenCompleteYourOrderButtonIsPressed_shoppingCartIsEmptied_andShoppingCartBadgeDisappears() {
        assertEquals(header.getCartCounter(), "1");
        orderSummaryPage.completeYourOrder();
        assertFalse(orderCompletePage.isShoppingCartBadgeDisplayed());
    }

    @Test(description = "On the Order summary page, when Cancel button is pressed, user is taken back to Cart page",
            testName = "Cancel button reverts to Cart page", priority = 1, suiteName = "Order summary test")
    public void whenCancelButtonIsPressed_cartPageIsDisplayed() {
        orderSummaryPage.cancel();
        assertEquals(mainPage.getSubHeaderText(), SUBHEADER_TEXT_CART);
    }

}
