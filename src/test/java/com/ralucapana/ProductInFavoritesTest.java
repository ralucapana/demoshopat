package com.ralucapana;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

public class ProductInFavoritesTest {

    public static final String AWESOME_GRANITE_CHIPS = "Awesome Granite Chips";
    private final MainPage mainPage = new MainPage();
    private final Header header = new Header();
    private final Footer footer = new Footer();
    private final Product p1 = new Product("1");
    private final Product p2 = new Product("3");
    private final ProductInFavorites productInFavorites = new ProductInFavorites("1");

    @BeforeMethod(description = "Before running tests, main page must be open")
    public void setup() {
        mainPage.openPage();
    }

    @AfterMethod(description = "Between tests, the page is being reset")
    public void cleanup() {
        Selenide.refresh();
        footer.resetPage();
    }

    @Test(description = "When using add to favorites button, product is added to Favorites page",
            testName = "Add to favorites", priority = 2, suiteName = "Favorites Test")
    public void whenAddingAProductToFavorites_productIsAddedInWishlist() {
        p1.addToFavorites();
        header.clickOnTheFavoritesButton();
        assertEquals(productInFavorites.getProductName(), AWESOME_GRANITE_CHIPS);
    }

    @Test(description = "When clicking on broken heart icon on a product from favorites, product is removed from Favorites page",
            testName = "Remove from favorites", priority = 2, suiteName = "Favorites Test")
    public void whenThereIsProductInFavoritesPage_andBrokenHeartIconIsClickedOn_productIsRemovedFromFavorites() {
        p1.addToFavorites();
        header.clickOnTheFavoritesButton();
        assertEquals(header.getFavoritesCounter(), "1");
        productInFavorites.removeFromFavorites();
        assertFalse(productInFavorites.isParentCardDisplayed());
    }

    @Test(description = "When clicking on broken heart icon on a product from favorites, product is removed from Favorites page and" +
            "Favorites icon badge disappears",
            testName = "Remove from favorites2", priority = 2, suiteName = "Favorites Test")
    public void whenThereIsProductInFavoritesPage_andBrokenHeartIconIsClickedOn_productIsRemovedFromFavorites2() {
        p1.addToFavorites();
        header.clickOnTheFavoritesButton();
        assertEquals(header.getFavoritesCounter(), "1");
        productInFavorites.removeFromFavorites();
        assertFalse(header.isFavoritesIconBadgeDisplayed());
    }

    @Test(description = "When there are products added to Favorites and reset button is clicked, products are removed from Favorites page",
            testName = "Remove from favorites3", priority = 2, suiteName = "Favorites Test")
    public void whenThereAreProductsInFavoritesPage_andResetButtonIsClickedOn_productsAreRemovedFromFavorites() {
        p1.addToFavorites();
        p2.addToFavorites();
        header.clickOnTheFavoritesButton();
        assertEquals(header.getFavoritesCounter(), "2");
        footer.resetPage();
        assertFalse(productInFavorites.isParentCardDisplayed());
    }

}

