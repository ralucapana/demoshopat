package com.ralucapana;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CheckOutPageTest {

    public static final String MISSING_LAST_NAME_MESSAGE = "Last Name is required";
    public static final String MISSING_FIRST_NAME_MESSAGE = "First Name is required";
    public static final String FIRST_NAME = "Dino";
    public static final String LAST_NAME = "Dinescu";
    public static final String MISSING_ADDRESS_MESSAGE = "Address is required";
    public static final String ADDRESS = "1st Linden St.";
    public static final String SUBHEADER_TEXT_SUMMARY = "Order summary";
    public static final String SUBHEADER_TEXT_CART = "Your cart";
    private final MainPage mainPage = new MainPage();
    private final Header header = new Header();
    private final Product p1 = new Product("1");
    private final CartPage cartPage = new CartPage();
    private final CheckOutPage checkOutPage = new CheckOutPage();
    private final Footer footer = new Footer();

    @BeforeMethod(description = "Before running tests, main page must be open")
    public void setup() {
        mainPage.openPage();
    }

    @AfterMethod(description = "Between tests, the page is being reset")
    public void cleanup() {
        Selenide.refresh();
        footer.resetPage();
    }

    @Test(description = "On the check out page, fill form for first name field is displayed",
            testName = "First name field is displayed", priority = 1, suiteName = "Check out Page Tests")
    public void whenCheckOutPageIsOpen_firstNameFieldIsDisplayed() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        cartPage.checkOut();
        assertTrue(checkOutPage.isFirstNameFieldDisplayed());
    }

    @Test(description = "On the check out page, fill form for last name field is displayed",
            testName = "Last name field is displayed", priority = 1, suiteName = "Check out Page Tests")
    public void whenCheckOutPageIsOpen_lastNameFieldIsDisplayed() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        cartPage.checkOut();
        assertTrue(checkOutPage.isLastNameFieldDisplayed());
    }

    @Test(description = "On the check out page, fill form for address field is displayed",
            testName = "Address field is displayed", priority = 1, suiteName = "Check out Page Tests")
    public void whenCheckOutPageIsOpen_addressFieldIsDisplayed() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        cartPage.checkOut();
        assertTrue(checkOutPage.isAddressFieldDisplayed());
    }

    @Test(description = "On the check out page, when typing in only first name, a message about missing last name is generated",
            testName = "Type in only first name", priority = 1, suiteName = "Check out Page Tests")
    public void whenFillingInOnlyFirstName_onCheckOutPage_instructionMessageAppears() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        cartPage.checkOut();
        checkOutPage.fillInFirstName(FIRST_NAME);
        checkOutPage.clickOnContinueCheckOutButton();
        assertEquals(checkOutPage.getInstructionMessage(), MISSING_LAST_NAME_MESSAGE);
    }

    @Test(description = "On the check out page, when typing in only last name, a message about missing first name is generated",
            testName = "Type in only last name", priority = 1, suiteName = "Check out Page Tests")
    public void whenFillingInOnlyLastName_onCheckOutPage_instructionMessageAppears() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        cartPage.checkOut();
        checkOutPage.fillInLastName(LAST_NAME);
        checkOutPage.clickOnContinueCheckOutButton();
        assertEquals(checkOutPage.getInstructionMessage(), MISSING_FIRST_NAME_MESSAGE);
    }

    @Test(description = "On the check out page, when typing in only first and last name, a message about missing address is generated",
            testName = "Type in only first and last name", priority = 1, suiteName = "Check out Page Tests")
    public void whenFillingInOnlyCompleteName_onCheckOutPage_instructionMessageAppears() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        cartPage.checkOut();
        checkOutPage.fillInFirstName(FIRST_NAME);
        checkOutPage.fillInLastName(LAST_NAME);
        checkOutPage.clickOnContinueCheckOutButton();
        assertEquals(checkOutPage.getInstructionMessage(), MISSING_ADDRESS_MESSAGE);
    }

    @Test(description = "On the check out page, when typing in only the address, a message about missing first name is generated",
            testName = "Type in only the address", priority = 1, suiteName = "Check out Page Tests")
    public void whenFillingInOnlyAddress_onCheckOutPage_instructionMessageAppears() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        cartPage.checkOut();
        checkOutPage.fillInAddress(ADDRESS);
        checkOutPage.clickOnContinueCheckOutButton();
        assertEquals(checkOutPage.getInstructionMessage(), MISSING_FIRST_NAME_MESSAGE);
    }

    @Test(description = "On the check out page, after typing in complete details and pressing the continue check out button, the check out process is successful",
            testName = "Successful check out", priority = 1, suiteName = "Check out Page Tests")
    public void whenFillingInCompleteDetails_onCheckOutPage_checkOutProcessIsSuccessful() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        cartPage.checkOut();
        checkOutPage.fillInFirstName(FIRST_NAME);
        checkOutPage.fillInLastName(LAST_NAME);
        checkOutPage.fillInAddress(ADDRESS);
        checkOutPage.clickOnContinueCheckOutButton();
        assertEquals(mainPage.getSubHeaderText(), SUBHEADER_TEXT_SUMMARY);
    }

    @Test(description = "On the check out page, when typing in only first name, a message about missing last name is generated",
            testName = "Type in only first name", priority = 1, suiteName = "Check out Page Tests")
    public void whenCancelButtonIsPressedInCheckOutPage_cartPageIsDisplayed() {
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        cartPage.checkOut();
        checkOutPage.clickOnCancelButton();
        assertEquals(mainPage.getSubHeaderText(), SUBHEADER_TEXT_CART);
    }

}
