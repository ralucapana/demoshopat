package com.ralucapana;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class OrderCompleteTest {

    public static final String FIRST_NAME = "Dino";
    public static final String LAST_NAME = "Dinescu";
    public static final String ADDRESS = "1st Linden St.";
    public static final String MAIN_PAGE_TITLE = "Demo shop";
    private final MainPage mainPage = new MainPage();
    private final Header header = new Header();
    private final CartPage cartPage = new CartPage();
    private final CheckOutPage checkOutPage = new CheckOutPage();
    private final Footer footer = new Footer();
    private final Product p1 = new Product("1");
    private final OrderCompletePage orderCompletePage = new OrderCompletePage();

    @BeforeMethod(description = "Before running test, an order must be placed and Continue check out button pressed")
    public void setup() {
        mainPage.openPage();
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        cartPage.checkOut();
        checkOutPage.fillInFirstName(FIRST_NAME);
        checkOutPage.fillInLastName(LAST_NAME);
        checkOutPage.fillInAddress(ADDRESS);
        checkOutPage.clickOnContinueCheckOutButton();
    }

    @AfterMethod(description = "After tests, page must be reset")
    public void cleanup() {
        Selenide.refresh();
        footer.resetPage();
    }

    @Test(description = "After successfully placing an order, when Continue shopping button is pressed, user is returned to the product list page",
            testName = "Order process successfully finished", priority = 2, suiteName = "Order finished Test")
    public void whenContinueShoppingButtonIsPressed_mainPageIsDisplayed() {
        orderCompletePage.pressContinueShoppingButton();
        assertEquals(mainPage.getPageTitle(), MAIN_PAGE_TITLE);
    }
}
