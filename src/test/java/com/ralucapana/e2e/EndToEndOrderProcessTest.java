package com.ralucapana.e2e;

import com.codeborne.selenide.Selenide;
import com.ralucapana.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class EndToEndOrderProcessTest {
    public static final String VALID_USERNAME = "dino";
    public static final String VALID_PASSWORD = "choochoo";
    public static final String GREETINGS_MESSAGE = "Hi dino!";
    public static final String FIRST_NAME = "Dino";
    public static final String LAST_NAME = "Dinescu";
    public static final String ADDRESS = "1st Linden St.";
    public static final String SUBHEADER_TEXT_SUMMARY = "Order summary";
    public static final String THANK_YOU_MESSAGE = "Thank you for your order!";
    public static final String MAIN_PAGE_TITLE = "Demo shop";
    public static final String GENERAL_GREETINGS_MESSAGE = "Hello guest!";
    private final MainPage mainPage = new MainPage();
    private final Header header = new Header();
    private final Footer footer = new Footer();
    private final LoginModal loginModal = new LoginModal();
    private final Product p1 = new Product("1");
    private final CartPage cartPage = new CartPage();
    private final CheckOutPage checkOutPage = new CheckOutPage();
    private final OrderSummaryPage orderSummaryPage = new OrderSummaryPage();
    private final OrderCompletePage orderCompletePage = new OrderCompletePage();

    @BeforeMethod(description = "Before running tests, main page must be open")
    public void setup() {
        mainPage.openPage();
    }

    @AfterMethod(description = "Between tests, the page is being reset")
    public void cleanup() {
        Selenide.refresh();
        footer.resetPage();
    }

    @Test(description = "Complete steps for successfully ordering a product", testName = "Successful complete order",
            priority = 1, suiteName = "Complete order")
    public void endToEndOrderProcess() {
        header.clickOnTheLoginIcon();
        loginModal.typeInUsername(VALID_USERNAME);
        loginModal.typeInPassword(VALID_PASSWORD);
        loginModal.clickOnTheLoginButton();
        assertEquals(header.getGreetingsMessage(), GREETINGS_MESSAGE);
        p1.addToCart();
        header.clickOnTheShoppingCartButton();
        assertTrue(cartPage.isItemsTotalDisplayed());
        cartPage.checkOut();
        checkOutPage.fillInFirstName(FIRST_NAME);
        checkOutPage.fillInLastName(LAST_NAME);
        checkOutPage.fillInAddress(ADDRESS);
        checkOutPage.clickOnContinueCheckOutButton();
        assertEquals(mainPage.getSubHeaderText(), SUBHEADER_TEXT_SUMMARY);
        orderSummaryPage.completeYourOrder();
        assertEquals(orderCompletePage.getThankYouMessage(), THANK_YOU_MESSAGE);
        orderCompletePage.pressContinueShoppingButton();
        assertEquals(mainPage.getPageTitle(), MAIN_PAGE_TITLE);
        header.clickOnTheLogoutButton();
        assertEquals(header.getGreetingsMessage(), GENERAL_GREETINGS_MESSAGE);

    }
}
