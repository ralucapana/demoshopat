## Raluca Pana 

Automated Testing for DemoShop Application - Presentation Project

### Developing Stack
-Java 17

-IntelliJ IDEA

-Maven

-Allure

-Selenide

-Selenium

-JUnit

-TestNG


### Implemented Tests

- MAIN PAGE
  - Open the web application
- AUTHENTICATION
    - Login functionality
    - Logout functionality
- HEADER
    - Button functionalities and redirects
    - Cart/Favorites icon badges
- FOOTER
    - Button functionalities and redirects
- PRODUCT LIST PAGE
    - Product display in List Page
    - Search functionality
    - Sorting functionality
    - Add to Cart/Add to Favorites functionalities
- CART PAGE
    - Update cart/cart items
    - Proceed to checkout functionality
- CHECKOUT PAGE
    - Form fill in functionality
    - Proceed to Order summary
- ORDER SUMMARY PAGE
    - Order info display
    - Complete order functionality
